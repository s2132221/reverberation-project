#!/usr/bin/python3

import numpy as np
import wavio
from scipy import signal

from matplotlib import pyplot as plt

import argparse


def load_wave_data(filename):
    """
    Load the wave data and time codes from the file with the given name.

    Args:
        - filename: The name of the input file

    Returns:
        - tt: Array conaining the correct time codes
        - wav_data: The normalised amplitudes of the waves
        - d_t: The time between samples
    """
    wav = wavio.read(filename)

    samp_rate = wav.rate
    samp_len = wav.data.shape[0]
    t_end = samp_len / samp_rate

    tt, d_t = np.linspace(0, t_end, samp_len, endpoint=False, retstep=True)

    # Bit depth in bits (not bytes)
    bits = 8 * wav.sampwidth

    # normalisation, maximum amplitude given by the highest number that can be
    # described by an integer with 'bits' bits.
    wav_data = wav.data[:, 0] / (2 ** (bits - 1) - 1)

    return tt, wav_data, d_t


def amp_to_db(data):
    """
    Convert amplitude data to decibels
    """
    return 20 * np.log10(data)


def crop_wave(tt, wave, t_start, t_end):
    """
    Return a cropped part of the wave between the time codes t_start and t_end
    """
    start_idx = np.argmin(np.abs(tt - t_start))
    end_idx = np.argmin(np.abs(tt - t_end)) + 1

    return tt[start_idx:end_idx], wave[start_idx:end_idx]


def fit_t60(tt, wave):
    """
    Fit the T60 reverberation time for the given time and waveform data
    """
    peaks, _ = signal.find_peaks(20 * np.log10(np.abs(wave)), width=4)

    ax = plt.gca()
    ax.plot(
        tt, 10 * np.log10(wave ** 2), c="k", alpha=0.3, linewidth=1, zorder=-1
    )

    mm, bb = np.polyfit(tt[peaks], 10 * np.log10(wave[peaks] ** 2), 1)
    # xx = np.arange(len(wave))
    ax.plot(tt, (bb + mm * tt), c="r", label="linear fit")
    ax.set_xlabel("time in s")
    ax.set_ylabel(r"$I$ in dB")
    ax.legend()
    plt.show()

    reverb_time = -60 / mm
    print("Reverberation time: {}s".format(reverb_time))
    return reverb_time


def process_input(filename, outname):
    """
    Load an input file of the format:

    filename    t_start    t_end

    and calculate the reverberation times for each given input wave file.
    Write the results to a file named `outname`.
    """
    with open(filename) as f:
        lines = f.readlines()

    reverb_times = []
    filenames = []
    for line in lines:
        split = line.split("    ")
        if len(split) <= 2:
            continue
        if split[0][0] == "#":
            continue
        filename = split[0].strip()
        t_start = float(split[1].strip())
        t_end = float(split[2].strip())
        print("Filename: {}:\t{}\t{}".format(filename, t_start, t_end))

        tt, wave, _ = load_wave_data(filename)
        tt_crop, wav_crop = crop_wave(tt, wave, t_start, t_end)
        plt.plot(tt_crop, wav_crop, c="k")
        plt.xlabel(r"$t$ in s ")
        plt.ylabel(r"Amplitude (normalised units)")
        plt.show()
        filenames.append(filename)
        reverb_times.append(fit_t60(tt_crop, wav_crop))

    lines = [
        "\t".join([fn, str(rt)]) + "\n"
        for fn, rt in zip(filenames, reverb_times)
    ]
    with open(outname, "w") as f:
        f.writelines(lines)


def main():
    """
    Parse command line arguments and process input
    """
    parser = argparse.ArgumentParser(
        "Script to process sound recordings to find a room's reverberation time"
    )

    msg = "The location of the input file. Default: 'all_files.txt'"
    parser.add_argument("-i", "--input", help=msg, default="all_files.txt")

    msg = "The location of the output file. Default: 'output.txt'"
    parser.add_argument("-o", "--output", help=msg, default="output.txt")

    args = parser.parse_args()

    process_input(args.input, args.output)


if __name__ == "__main__":
    main()
