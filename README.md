# Reverberation time

Script to evaluate the measurements taken as part of project 1 in the musical
acoustics lecture at the University of Edinburgh in Semester 1 2020/2021.
The included script `reverberation_time.py` takes an input file of the form
```
  FILENAME    T_START    T_END
```
where `FILENAME` is the corresponding `.wav` file and `T_START` and `T_END` are
the start and end time codes in seconds from the beginning of the file. The
program will carry out a linear fit of the sound pressure in dB between these
two time points and thereby calculate the reverberation time.

To run the script for all sample data files, simply type

```
python3 reverberation_time.py
```

without further arguments. To get a list of command line arguments, run

```
python3 reverberation_time.py --help
```
